package util

import (
	"fmt"
	"math/rand"
	"time"
)

type UserOtp struct {
	Otp string `json:"otp"`
}

const (
	Numeric = "0123456789"
)

func RandNumeric() string {
	source := rand.NewSource(time.Now().UnixNano())
	random := rand.New(source)
	min := 10000
	max := 99999
	randomNum := random.Intn(max-min+1) + min
	return fmt.Sprintf("%05d", randomNum)
}
