package src

import (
	"fmt"
	"go-otp/config"
	"go-otp/src/delivery"
	"go-otp/src/repository"
	"go-otp/src/usecase"
	"log"
	"net/http"
)

type Server struct {
	httpServer *http.Server
	cfg        config.Config
}

func InitServer(cfg config.Config) *Server {
	mux := http.NewServeMux()

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, World! %s %s", cfg.ServiceName(), cfg.ServiceEnvironment())
	})

	postRepo := repository.NewPostRepository(cfg)
	postUsecase := usecase.NewPostUsecase(postRepo)
	postDelivery := delivery.NewPostDelivery(postUsecase)
	postDelivery.Mount(mux, "/posts")

	userRepo := repository.NewUserRepository(cfg)
	userUsecase := usecase.NewUserUsecase(cfg, userRepo)
	userDelivery := delivery.NewUserDelivery(userUsecase)
	userDelivery.Mount(mux, "/users")

	return &Server{
		httpServer: &http.Server{
			Addr:    fmt.Sprintf(":%d", cfg.ServicePort()),
			Handler: mux,
		},
		cfg: cfg,
	}
}

func (s *Server) Run() {
	log.Fatal(s.httpServer.ListenAndServe())
}
