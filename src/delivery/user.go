package delivery

import (
	"encoding/json"
	"go-otp/src/model"
	"go-otp/src/request"
	"net/http"
)

type userDelivery struct {
	userUsecase model.UserUsecase
}

func NewUserDelivery(userUsecase model.UserUsecase) *userDelivery {
	return &userDelivery{userUsecase: userUsecase}
}

func (ud *userDelivery) Mount(mux *http.ServeMux, path string) {
	mux.HandleFunc(path+"/register", ud.registerHandler)
	mux.HandleFunc(path+"/request-otp", ud.requestOtpHandler)
	mux.HandleFunc(path+"/login", ud.loginHandler)
}

func (ud *userDelivery) requestOtpHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var req request.GetOtpRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = ud.userUsecase.RequestOtp(ctx, req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"status": "success",
		"code":   http.StatusOK,
	})
}

func (ud *userDelivery) loginHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var req request.LoginWithOTPRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	otp, err := ud.userUsecase.LoginWithOTP(ctx, req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"status": "success",
		"code":   http.StatusOK,
		"data":   otp,
	})
}

func (ud *userDelivery) registerHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var req request.CreateUserRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	userList, err := ud.userUsecase.CreateUser(ctx, req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(userList)
}