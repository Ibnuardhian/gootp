package delivery

import (
	"encoding/json"
	"go-otp/src/model"
	"go-otp/src/request"
	"net/http"
	"strconv"
)

type PostDelivery struct {
	postUsecase model.PostUsecase
}

func NewPostDelivery(postUsecase model.PostUsecase) *PostDelivery {
	return &PostDelivery{postUsecase: postUsecase}
}

func (p *PostDelivery) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		p.GetPostHandler(w, r)
	case http.MethodPost:
		p.StorePostHandler(w, r)
	default:
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

func (p *PostDelivery) GetPostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	limit := r.URL.Query().Get("limit")
	offset := r.URL.Query().Get("offset")

	limitInt, _ := strconv.Atoi(limit)
	offsetInt, _ := strconv.Atoi(offset)

	postList, err := p.postUsecase.GetPostList(ctx, limitInt, offsetInt)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(postList)
}

func (p *PostDelivery) StorePostHandler(w http.ResponseWriter, r *http.Request) {
	var req request.PostRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(true)
}

func (p *PostDelivery) Mount(mux *http.ServeMux, path string) {
	mux.Handle(path, p)
}
