package middleware

import (
	"net/http"

	"github.com/go-playground/validator"
)

type GoPlaygroundValidator struct {
	Validator *validator.Validate
}

func NewGoPlaygroundValidator() *GoPlaygroundValidator {
	return &GoPlaygroundValidator{
		Validator: validator.New(),
	}
}

func (gp *GoPlaygroundValidator) Validate(i interface{}) error {
	if err := gp.Validator.Struct(i); err != nil {
		return &HTTPError{
			Code:    http.StatusBadRequest,
			Message: err.Error(),
		}
	}
	return nil
}

type HTTPError struct {
	Code    int
	Message string
}

func (e *HTTPError) Error() string {
	return e.Message
}
