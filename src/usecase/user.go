package usecase

import (
	"context"
	"errors"
	"go-otp/config"
	"go-otp/mail"
	"go-otp/src/model"
	"go-otp/src/request"
	"go-otp/util"
	"go-otp/util/token"
)

type userUsecase struct {
	cfg      config.Config
	userRepo model.UserRepository
}

func NewUserUsecase(cfg config.Config, user model.UserRepository) model.UserUsecase {
	return &userUsecase{
		cfg:      cfg,
		userRepo: user,
	}
}

func (u *userUsecase) CreateUser(ctx context.Context, request request.CreateUserRequest) (*model.User, error) {
	hashPassword, _ := util.NewPassword(request.Password)

	user, err := u.userRepo.Create(ctx, &model.User{
		Name:     request.Name,
		Email:    request.Email,
		Password: hashPassword,
	})
	if err != nil {
		return nil, err
	}

	//send email
	go u.cfg.GoMail().SendEmail(ctx, mail.SendEmailRequest{
		To:      user.Email,
		Body:    "Berikut ini password anda : " + request.Password,
		Subject: "Selamat Datang di Ibn App",
	})

	return user, nil
}

func (u *userUsecase) RequestOtp(ctx context.Context, request request.GetOtpRequest) error {
	user, err := u.userRepo.FindByEmail(ctx, request.Email)
	if err != nil {
		return err
	}

	otp := util.RandNumeric()
	cacheKey := "otp:" + user.Email
	err = u.cfg.Redis().Set(ctx, cacheKey, otp)
	if err != nil {
		return err
	}

	go u.cfg.GoMail().SendEmail(ctx, mail.SendEmailRequest{
		To:      user.Email,
		Body:    "Berikut ini adalah OTP Anda : " + otp,
		Subject: "OTP dari Ibn App",
	})

	return nil
}

func (u *userUsecase) LoginWithOTP(ctx context.Context, request request.LoginWithOTPRequest) (*token.NewTokenResponse, error) {
	cacheKey := "otp:" + request.Email

	otpValue, _ := u.cfg.Redis().Get(ctx, cacheKey)

	if request.Otp != otpValue {
		return nil, errors.New("Otp invalid")
	}

	user, err := u.userRepo.FindByEmail(ctx, request.Email)
	if err != nil {
		return nil, err
	}

	token, err := token.NewCustomToken(token.NewTokenRequest{
		UserID:    user.ID,
		UserEmail: user.Email,
	}, token.DurationShort)
	if err != nil {
		return nil, err
	}

	return token, nil
}
