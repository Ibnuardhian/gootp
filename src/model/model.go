package model

import (
	"context"
	"go-otp/src/request"
	"go-otp/src/response"
	"go-otp/util/token"
	"time"
)

type User struct {
	ID        int       `json:"id"`
	Name      string    `json:"username"`
	Email     string    `json:"email" gorm:"unique"`
	Password  string    `json:"password"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type Post struct {
	ID        int       `json:"id"`
	Title     string    `json:"title"`
	Slug      string    `json:"slug"`
	Content   string    `json:"content"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type UserRepository interface {
	Create(ctx context.Context, user *User) (*User, error)
	FindByEmail(ctx context.Context, email string) (*User, error)
}

type UserUsecase interface {
	RequestOtp(ctx context.Context, request request.GetOtpRequest) error
	CreateUser(ctx context.Context, request request.CreateUserRequest) (*User, error)
	LoginWithOTP(ctx context.Context, request request.LoginWithOTPRequest) (*token.NewTokenResponse, error)
}

type PostRepository interface {
	Create(ctx context.Context, post *Post) (*Post, error)
	FindByID(ctx context.Context, id int) (*Post, error)
	Get(ctx context.Context, limit, offset int) ([]*Post, int64, error)
}

type PostUsecase interface {
	GetPostList(ctx context.Context, limit, offset int) (*response.PostsResponse, error)
}
