package repository

import (
	"context"
	"encoding/json"
	"errors"
	"go-otp/config"
	"go-otp/src/model"
	"strconv"
)

type postRepository struct {
	Cfg config.Config
}

func NewPostRepository(cfg config.Config) model.PostRepository {
	return &postRepository{Cfg: cfg}
}

func (p *postRepository) FindByID(ctx context.Context, id int) (*model.Post, error) {
	query := "SELECT id, title, slug, content, created_at, updated_at FROM posts WHERE id = ?"
	rows, err := p.Cfg.Database().WithContext(ctx).Raw(query, id).Rows()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var post model.Post
	if rows.Next() {
		if err := rows.Scan(&post.ID, &post.Title, &post.Slug, &post.Content, &post.CreatedAt, &post.UpdatedAt); err != nil {
			return nil, err
		}
	} else {
		return nil, errors.New("post not found")
	}
	return &post, nil
}

func (p *postRepository) Create(ctx context.Context, post *model.Post) (*model.Post, error) {
	if err := p.Cfg.Database().WithContext(ctx).Create(&post).Error; err != nil {
		return nil, err
	}
	return post, nil
}

func (p *postRepository) UpdateByID(ctx context.Context, id int, post *model.Post) (*model.Post, error) {
	if err := p.Cfg.Database().WithContext(ctx).
		Model(&model.Post{ID: id}).Updates(post).Find(post).Error; err != nil {
		return nil, err
	}

	return post, nil
}

func (p *postRepository) Delete(ctx context.Context, id int) error {
	err := p.Cfg.Database().WithContext(ctx).Delete(&model.Post{ID: id}).Error
	if err != nil {
		return err
	}
	return nil
}

func (p *postRepository) Get(ctx context.Context, limit, offset int) ([]*model.Post, int64, error) {
	var data []*model.Post
	var count int64
	p.Cfg.Database().WithContext(ctx).Model(&model.Post{}).Count(&count)

	key := "article:limit" + strconv.Itoa(limit) + ":offset:" + strconv.Itoa(offset)
	posts, err := p.Cfg.Redis().Get(ctx, key)
	if err != nil {
		if err := p.Cfg.Database().WithContext(ctx).
			Select("id", "title", "slug", "content", "created_at", "updated_at").
			Limit(limit).Offset(offset).Find(&data).Error; err != nil {
			return nil, 0, err
		}

		err = p.Cfg.Redis().Set(ctx, key, data)
		if err != nil {
			return nil, 0, err
		}

		return data, count, nil
	}

	err = json.Unmarshal([]byte(posts), &data)
	if err != nil {
		return nil, 0, err
	}

	return data, count, nil
}
