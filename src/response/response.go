package response

type Meta struct {
	Total  int64 `json:"total"`
	Limit  int   `json:"limit"`
	Offset int   `json:"offset"`
}

type PostsResponse struct {
	Post interface{} `json:"data"`
	Meta Meta        `json:"meta"`
}

type PostResponse struct {
	Post interface{} `json:"data"`
}
