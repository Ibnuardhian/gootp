package postgres

import (
	"go-otp/src/model"
	"os"

	"github.com/rs/zerolog/log"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func InitGorm() *gorm.DB {

	connection := os.Getenv("DATABASE_URL")

	db, err := gorm.Open(postgres.Open(connection))
	if err != nil {
		log.Error().Msgf("can't connect to database %s", err)
	}
	db.AutoMigrate(&model.User{})

	return db

}
