package config

import (
	"go-otp/config/cache"
	"go-otp/config/postgres"
	"go-otp/mail"
	"os"
	"strconv"

	"gorm.io/gorm"
)

type (
	config struct {
	}

	Config interface {
		GoMail() mail.EmailSmtp
		ServiceName() string
		ServicePort() int
		ServiceEnvironment() string
		Database() *gorm.DB
		Redis() cache.Redis
	}
)

func (c *config) GoMail() mail.EmailSmtp {
	return mail.NewEmailSmtp()
}

func (c *config) Redis() cache.Redis {
	return cache.InitRedis()
}

func NewConfig() Config {
	return &config{}
}

func (c *config) Database() *gorm.DB {
	return postgres.InitGorm()
}

func (c *config) ServiceName() string {
	return os.Getenv("SERVICE_NAME")
}

func (c *config) ServicePort() int {
	v := os.Getenv("PORT")
	port, _ := strconv.Atoi(v)

	return port
}

func (c *config) ServiceEnvironment() string {
	return os.Getenv("ENV")
}
