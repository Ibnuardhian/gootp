package cache

import (
	"context"
	"testing"
	"time"

	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/assert"
)

var client = redis.NewClient(&redis.Options{
	Addr: "localhost:6379",
	DB:   0,
})

func TestConnection(t *testing.T) {
	assert.NotNil(t, client)
	// err := client.Close()
	// assert.Nil(t, err)
}

var ctx = context.Background()

func TestPing(t *testing.T) {
	result, err := client.Ping(ctx).Result()
	assert.Nil(t, err)
	assert.Equal(t, "PONG", result)
}

func TestString(t *testing.T) {
	client.SetEx(ctx, "name", "ibn", time.Second*3)
	result, err := client.Get(ctx, "name").Result()
	assert.Nil(t, err)
	assert.Equal(t, "ibn", result)

	time.Sleep(5 * time.Second)

	result, err = client.Get(ctx, "name").Result()
	assert.NotNil(t, err)
}

func TestList(t *testing.T) {
	client.RPush(ctx, "names", "Muhammad")
	client.RPush(ctx, "names", "Ibnu")
	client.RPush(ctx, "names", "Ardhian")

	assert.Equal(t, "Muhammad", client.LPop(ctx, "names").Val())
	assert.Equal(t, "Ibnu", client.LPop(ctx, "names").Val())
	assert.Equal(t, "Ardhian", client.LPop(ctx, "names").Val())
	
	client.Del(ctx, "names")
}

func TestSet(t *testing.T) {
	client.SAdd(ctx, "students", "Muhammad")
	client.SAdd(ctx, "students", "Muhammad")
	client.SAdd(ctx, "students", "Ibnu")
	client.SAdd(ctx, "students", "Ibnu")
	client.SAdd(ctx, "students", "Ardhian")
	client.SAdd(ctx, "students", "Ardhian")

	assert.Equal(t, int64(3), client.SCard(ctx, "students").Val())
	assert.Equal(t, []string{"Muhammad", "Ibnu", "Ardhian"}, client.SMembers(ctx, "students").Val())

}
