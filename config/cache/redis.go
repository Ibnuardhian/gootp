package cache

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/redis/go-redis/v9"
)

func InitRedis() Redis {
	host := os.Getenv("REDIS_HOST")
	port := os.Getenv("REDIS_PORT")
	password := os.Getenv("REDIS_PASSWORD")

	if host == "" {
		host = "localhost"
	}
	if port == "" {
		port = "6379"
	}

	opt := &redis.Options{
		Addr:     fmt.Sprintf("%s:%s", host, port),
		Password: password,
		DB:       0,
	}

	rdb := redis.NewClient(opt)
	return &redisClient{rdb: rdb}
}

type redisClient struct {
	rdb *redis.Client
}

type Redis interface {
	Set(ctx context.Context, key string, value interface{}) error
	Get(ctx context.Context, key string) (string, error)
}

func (c *redisClient) Set(ctx context.Context, key string, value interface{}) error {
	err := c.rdb.Set(ctx, key, value, 5*time.Minute).Err()
	return err
}

func (c *redisClient) Get(ctx context.Context, key string) (string, error) {
	val, err := c.rdb.Get(ctx, key).Result()
	if err != nil {
		return "", err
	}
	return val, nil
}

func (c *redisClient) IsConnected(ctx context.Context, key string) bool {
	_, err := c.rdb.Ping(context.Background()).Result()
	if err != nil {
		return false
	}
	return true
}
